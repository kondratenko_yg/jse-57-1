package ru.kondratenko.tm.repository;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import ru.kondratenko.tm.entity.User;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class UserRepositoryIntegrationTest {

    private static final String NAME_FIND = "nameFind";
    private static final String FIRSTNAME_FIND = "firstNameFind";
    private static final String LASTNAME_FIND = "lastNameFind";
    private static final String NAME_DELETE = "nameDelete";
    private static final String NAME_UPDATE = "nameUpdate";
    private static final String NAME_UPDATE2 = "nameUpdate2";
    private static final String LASTNAME_UPDATE2 = "lastnameUpdate2";

    private static Optional<User> resultFind;
    private static Optional<User> resultDelete;
    private  static UserRepository userRepository;
    private static int counter = 0;

    @BeforeAll
    public static void setup() {
        userRepository = new UserRepository();
        User user = User.builder().name(NAME_FIND).firstName(FIRSTNAME_FIND).lastName(LASTNAME_FIND).build();
        User userDelete = User.builder().name(NAME_DELETE + counter++).firstName(FIRSTNAME_FIND).lastName(LASTNAME_FIND).build();
        resultFind = userRepository.create(user);
        resultDelete = userRepository.create(userDelete);
    }

    @Test
    void FindByID() {
        if (resultFind.isPresent()) {
            assertEquals(NAME_FIND, userRepository.findById(resultFind.get().getId()).get().getName());
            assertEquals(FIRSTNAME_FIND, userRepository.findById(resultFind.get().getId()).get().getFirstName());
            assertEquals(LASTNAME_FIND, userRepository.findById(resultFind.get().getId()).get().getLastName());
        } else {
            throw new RuntimeException();
        }
    }

    @Test
    void FindByIndex() {
        if (resultFind.isPresent()) {
            assertEquals(NAME_FIND, userRepository.findByIndex(1).get().getName());
            assertEquals(FIRSTNAME_FIND, userRepository.findByIndex(1).get().getFirstName());
            assertEquals(LASTNAME_FIND, userRepository.findByIndex(1).get().getLastName());
        } else {
            throw new RuntimeException();
        }
    }

    @Test
    void FindByName() {
        if (resultFind.isPresent()) {
            assertEquals(NAME_FIND, userRepository.findByName(NAME_FIND).get().getName());
            assertEquals(FIRSTNAME_FIND, userRepository.findByName(NAME_FIND).get().getFirstName());
            assertEquals(LASTNAME_FIND, userRepository.findByName(NAME_FIND).get().getLastName());
        } else {
            throw new RuntimeException();
        }
    }

    @Test
    void createAndDeleteById() {
        User user = User.builder().name(NAME_DELETE + counter++).firstName(FIRSTNAME_FIND).lastName(LASTNAME_FIND).build();
        Optional<User> result = userRepository.create(user);
        if (result.isPresent()) {
            Long userID = result.get().getId();
            userRepository.removeById(userID);
            assertFalse(userRepository.findById(userID).isPresent());
        } else {
            throw new RuntimeException();
        }
    }

    @Test
    void createAndDeleteByIndex() {
        if (resultDelete.isPresent()) {
            Long userID = resultDelete.get().getId();
            userRepository.removeByIndex(2);
            assertFalse(userRepository.findById(userID).isPresent());
        } else {
            throw new RuntimeException();
        }
    }

    @Test
    void createAndDeleteByName() {
        String name = NAME_DELETE + counter++;
        User user = User.builder().name(name).firstName(FIRSTNAME_FIND).lastName(LASTNAME_FIND).build();
        Optional<User> result = userRepository.create(user);
        if (result.isPresent()) {
            Long userID = result.get().getId();
            userRepository.removeByName(name);
            assertFalse(userRepository.findById(userID).isPresent());
        } else {
            throw new RuntimeException();
        }
    }

    @Test
    void createAndUpdate() {
        User user = User.builder().name(NAME_UPDATE).build();
        userRepository.create(user);
        user.setName(NAME_UPDATE2);
        user.setLastName(LASTNAME_UPDATE2);
        Optional<User> result = userRepository.update(user);
        if (result.isPresent()) {
            assertEquals(NAME_UPDATE2, userRepository.findById(result.get().getId()).get().getName());
            assertEquals(LASTNAME_UPDATE2, userRepository.findById(result.get().getId()).get().getLastName());
        } else {
            throw new RuntimeException();
        }
    }

    @AfterAll
    static void  ClearFindAll() {
        userRepository.clear();
        List<User> users = userRepository.findAll();
        assertEquals(0,users.size());
    }


}
