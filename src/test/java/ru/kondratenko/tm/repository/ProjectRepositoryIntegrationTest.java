package ru.kondratenko.tm.repository;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import ru.kondratenko.tm.entity.Project;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class ProjectRepositoryIntegrationTest {

    private static final String NAME_FIND = "nameFind";
    private static final String NAME_DELETE = "nameDelete";
    private static final String NAME_UPDATE = "nameUpdate";
    private static final String NAME_UPDATE2 = "nameUpdate2";
    private static final String DESCRIPTION_UPDATE2 = "descrictionUpdate2";

    private static Optional<Project> resultFind;
    private static Optional<Project> resultDelete;
    private  static ProjectRepository projectRepository;
    private static int counter = 0;

    @BeforeAll
    public static void setup() {
        projectRepository = new ProjectRepository();
        Project project = Project.builder().name(NAME_FIND).build();
        Project ProjectDelete = Project.builder().name(NAME_DELETE + counter++).build();
        resultFind = projectRepository.create(project);
        resultDelete = projectRepository.create(ProjectDelete);
    }

    @Test
    void FindByID() {
        if (resultFind.isPresent()) {
            assertEquals(NAME_FIND, projectRepository.findById(resultFind.get().getId()).get().getName());
        } else {
            throw new RuntimeException();
        }
    }

    @Test
    void FindByIndex() {
        if (resultFind.isPresent()) {
            assertEquals(NAME_FIND, projectRepository.findByIndex(1).get().getName());
        } else {
            throw new RuntimeException();
        }
    }

    @Test
    void FindByName() {
        if (resultFind.isPresent()) {
            assertEquals(NAME_FIND, projectRepository.findByName(NAME_FIND).get(0).getName());
        } else {
            throw new RuntimeException();
        }
    }

    @Test
    void createAndDeleteById() {
        Project project = Project.builder().name(NAME_DELETE + counter++).build();
        Optional<Project> result = projectRepository.create(project);
        if (result.isPresent()) {
            Long ProjectID = result.get().getId();
            projectRepository.removeById(ProjectID);
            assertFalse(projectRepository.findById(ProjectID).isPresent());
        } else {
            throw new RuntimeException();
        }
    }

    @Test
    void createAndDeleteByIndex() {
        if (resultDelete.isPresent()) {
            Long ProjectID = resultDelete.get().getId();
            projectRepository.removeByIndex(2);
            assertFalse(projectRepository.findById(ProjectID).isPresent());
        } else {
            throw new RuntimeException();
        }
    }

    @Test
    void createAndDeleteByName() {
        String name = NAME_DELETE + counter++;
        Project project = Project.builder().name(name).build();
        Optional<Project> result = projectRepository.create(project);
        if (result.isPresent()) {
            Long ProjectID = result.get().getId();
            projectRepository.removeByName(name);
            assertFalse(projectRepository.findById(ProjectID).isPresent());
        } else {
            throw new RuntimeException();
        }
    }

    @Test
    void createAndUpdate() {
        Project project = Project.builder().name(NAME_UPDATE).build();
        projectRepository.create(project);
        project.setName(NAME_UPDATE2);
        project.setDescription(DESCRIPTION_UPDATE2);
        Optional<Project> result = projectRepository.update(project);
        if (result.isPresent()) {
            assertEquals(NAME_UPDATE2, projectRepository.findById(result.get().getId()).get().getName());
            assertEquals(DESCRIPTION_UPDATE2, projectRepository.findById(result.get().getId()).get().getDescription());
        } else {
            throw new RuntimeException();
        }
    }

    @AfterAll
    static void  ClearFindAll() {
        projectRepository.clear();
        List<Project> Projects = projectRepository.findAll();
        assertEquals(0,Projects.size());

    }


}
