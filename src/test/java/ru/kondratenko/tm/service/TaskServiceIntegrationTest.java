package ru.kondratenko.tm.service;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import ru.kondratenko.tm.entity.Project;
import ru.kondratenko.tm.entity.Task;
import ru.kondratenko.tm.entity.User;
import ru.kondratenko.tm.repository.ProjectRepository;
import ru.kondratenko.tm.repository.TaskRepository;
import ru.kondratenko.tm.repository.UserRepository;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TaskServiceIntegrationTest { //jse-61

    private static User user;
    private static Project project;
    private  static TaskService taskService;
    private static Task task;

    @BeforeAll
    public static void setup() {
        taskService = new TaskService();
        TaskRepository taskRepository = new TaskRepository();
        taskService.setTaskRepository(taskRepository);
        UserRepository userRepository = new UserRepository();
        taskService.setUserRepository(userRepository);
        ProjectRepository projectRepository = new ProjectRepository();
        taskService.setProjectRepository(projectRepository);

        user = User.builder().name("user123").password("123").build();
        userRepository.create(user);

        project = Project.builder().name("project123").build();
        projectRepository.create(project);

        task = Task.builder().name("rrr").user(user).project(project).build();
        taskRepository.create(task);
    }

    @Test
    void findAllByUserId(){
        assertEquals(task.getName(),taskService.findAllByUserId(user.getId()).getPayloadTask()[0].getName());
    }

    @Test
    void findAllByProjectId(){
        assertEquals(task.getName(),taskService.findAllByProjectId(project.getId()).getPayloadTask()[0].getName());
    }

    @Test
    void findByProjectIdAndId(){
        assertEquals(task.getName(),taskService.findByProjectIdAndId(project.getId(),task.getId()).getPayloadTask().getName());
    }
}
