package ru.kondratenko.tm.controller;

import ru.kondratenko.tm.dto.response.list.ListTaskResponseDTO;
import ru.kondratenko.tm.dto.TaskDTO;
import ru.kondratenko.tm.dto.response.TaskResponseDTO;

public interface IControllerTask {

    TaskResponseDTO create(TaskDTO task);

    TaskResponseDTO updateById(Long id, TaskDTO task);

    TaskResponseDTO updateByIndex(Integer index, TaskDTO task);

    ListTaskResponseDTO listTaskByProjectId(Long projectId);

    TaskResponseDTO addTaskToProjectByIds(Long projectId, Long taskId);

    TaskResponseDTO removeTaskFromProjectByIds(Long projectId, Long taskId);

    ListTaskResponseDTO viewByName(String name);

    TaskResponseDTO viewById(Long id);

    TaskResponseDTO viewByIndex(Integer index);

    TaskResponseDTO removeByIndex(Integer index);

    TaskResponseDTO removeById(Long id);

    ListTaskResponseDTO list();

    TaskResponseDTO findByProjectIdAndId(final Long projectId, final Long id);

    ListTaskResponseDTO findAllByUserId(Long Id);
}
