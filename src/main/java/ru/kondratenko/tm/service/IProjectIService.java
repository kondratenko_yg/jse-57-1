package ru.kondratenko.tm.service;

import ru.kondratenko.tm.dto.response.list.ListProjectResponseDTO;
import ru.kondratenko.tm.dto.ProjectDTO;
import ru.kondratenko.tm.dto.response.ProjectResponseDTO;

public interface IProjectIService extends IService<ProjectDTO, ProjectResponseDTO, ListProjectResponseDTO> {
    ListProjectResponseDTO findAllByUserId(Long Id);
    ListProjectResponseDTO findByName(final String name);
    ProjectResponseDTO removeByName(final String name);
}
