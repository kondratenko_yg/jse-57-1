package ru.kondratenko.tm.repository;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;
import ru.kondratenko.tm.entity.Project;
import ru.kondratenko.tm.entity.Task;
import ru.kondratenko.tm.entity.User;

import javax.persistence.NoResultException;
import java.util.List;
import java.util.Optional;
@Repository
public class UserRepository extends CommonRepository<User> implements IUserRepository  {
    public User currentUser;

    public UserRepository() {
        super(User.class);
    }

    @Override
    public Optional<User> findByName(String name){
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        Optional<User> entity = Optional.empty();

        try {
            entity = Optional.ofNullable(session.createQuery("SELECT u FROM " + User.class.getName() + " u where u.name = :name", User.class)
                    .setParameter("name", name).getSingleResult());
        } catch (NoResultException e) {
        }

        tx.commit();
        session.close();

        return entity;
    }

    @Override
    public List<Task> findTasks(Long id) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        List<Task> tasks = session.createQuery("SELECT t FROM " + User.class.getName() + " u inner join u.tasks as t where u.id = :id", Task.class)
                    .setParameter("id", id).list();

        tx.commit();
        session.close();

        return tasks;
    }

    @Override
    public List<Project> findProjects(Long id) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        List<Project> projects = session.createQuery("SELECT t FROM " + User.class.getName() + " u inner join u.projects as t where u.id = :id", Project.class)
                .setParameter("id", id).list();

        tx.commit();
        session.close();

        return projects;
    }
}
