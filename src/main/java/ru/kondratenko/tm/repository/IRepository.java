package ru.kondratenko.tm.repository;

import java.util.List;
import java.util.Optional;

public interface IRepository<E> {

    Optional<E> create(final E item);

    Optional<E> findById(final Long id);

    List<E> findAll();

    void clear();

    Optional<E> findByIndex(final int index);

    void removeByIndex(final int index);

    void removeById(final Long id);

    void removeByName(final String name);

    Optional<E> update(final E user);
}
