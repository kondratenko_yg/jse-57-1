package ru.kondratenko.tm.repository;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;
import ru.kondratenko.tm.entity.Project;

import javax.persistence.NoResultException;
import java.util.List;
import java.util.Optional;
@Repository
public class ProjectRepository extends CommonRepository<Project> implements ITaskProjectRepository<Project> {

    public ProjectRepository() {
        super(Project.class);
    }

    @Override
    public List<Project> findByName(String name){
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();

        List<Project> entity = session.createQuery("SELECT u FROM " + Project.class.getName() + " u where u.name = :name", Project.class)
                .setParameter("name", name).getResultList();

        tx.commit();
        session.close();

        return entity;
    }

    @Override
    public List<Project> findAllByUserId(final Long id) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();

        List<Project> entity = session.createQuery("SELECT u FROM " + Project.class.getName() + " u inner join u.user as c_user where c_user.id = :id", Project.class)
                .setParameter("id", id).getResultList();

        tx.commit();
        session.close();

        return entity;
    }
    
}
