package ru.kondratenko.tm.repository;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import ru.kondratenko.tm.entity.Project;
import ru.kondratenko.tm.entity.Task;
import ru.kondratenko.tm.repository.config.HibernateConfig;

import javax.persistence.NoResultException;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

public class CommonRepository<T> implements IRepository<T> {

    public Class<T> clazz;

    public CommonRepository(Class<T> clazz) {
        this.clazz = clazz;
        this.sessionFactory = HibernateConfig.getSessionFactory();
    }

    public SessionFactory sessionFactory;

    @Override
    public  Optional<T> create(final T entity) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();

        session.save(entity);

        tx.commit();
        session.close();
        return Optional.ofNullable(entity);
    }

    @Override
    public Optional<T> findById(Long id) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();

        T entity = session.get(clazz, id);

        tx.commit();
        session.close();

        return Optional.ofNullable(entity);
    }

    @Override
    public List<T> findAll() {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();

        List<T> items = session.createQuery("SELECT c from "+clazz.getName()+" c", clazz).getResultList();

        tx.commit();
        session.close();

        return items;
    }

    @Override
    public void clear() {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();

        session.createQuery("DELETE FROM "+clazz.getName()).executeUpdate();

        tx.commit();
        session.close();
    }

    @Override
    public Optional<T> findByIndex(int index) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        Optional<T> entity = Optional.empty();

        try {
            entity = Optional.ofNullable(session.createNativeQuery("SELECT * FROM "+
            "( SELECT *, row_number()  OVER () as rnum from "+ clazz.getSimpleName().toLowerCase(Locale.ROOT)+"s" + ") as u where rnum = :index", clazz)
                    .setParameter("index", index).getSingleResult());
        } catch (NoResultException e) {
        }

        tx.commit();
        session.close();

        return entity;
    }

    @Override
    public void removeByIndex(int index) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        String tableName = clazz.getSimpleName().toLowerCase(Locale.ROOT)+"s";
        String idName = clazz.getSimpleName().toLowerCase(Locale.ROOT)+"_id";

        session.createNativeQuery("DELETE FROM "+tableName+" WHERE "+idName+" in "+
                    "(SELECT "+idName+" FROM (SELECT "+idName+", row_number()  OVER () as rnum from "+ tableName  + ") WHERE rnum = :index)")
                    .setParameter("index", index).executeUpdate();

        tx.commit();
        session.close();
    }

    @Override
    public void removeById(Long id) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();

        session.createQuery("DELETE from "+clazz.getName()+" c WHERE c.id=:id")
                .setParameter("id",id).executeUpdate();

        tx.commit();
        session.close();
    }

    @Override
    public Optional<T> update(T input) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        session.save(input);
        tx.commit();
        session.close();

        return Optional.ofNullable(input);
    }

    @Override
    public void removeByName(String name)  {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();

        session.createQuery("DELETE FROM " + clazz.getName() + " u where u.name = :name")
                .setParameter("name", name).executeUpdate();

        tx.commit();
        session.close();
    }
}
