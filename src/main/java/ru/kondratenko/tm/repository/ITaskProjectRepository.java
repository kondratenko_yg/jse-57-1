package ru.kondratenko.tm.repository;

import ru.kondratenko.tm.entity.Task;

import java.util.List;

public interface ITaskProjectRepository<E> extends IRepository<E> {
    List<E> findAllByUserId(final Long userId);
    List<E> findByName(final String login);
}
