package ru.kondratenko.tm.repository;

import ru.kondratenko.tm.entity.Project;
import ru.kondratenko.tm.entity.Task;
import ru.kondratenko.tm.entity.User;

import java.util.List;
import java.util.Optional;

public interface IUserRepository extends IRepository<User> {
    Optional<User> findByName(final String login);
    List<Task> findTasks(Long id);
    List<Project> findProjects(Long id);
}
