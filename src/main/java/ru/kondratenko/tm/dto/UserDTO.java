package ru.kondratenko.tm.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.kondratenko.tm.enumerated.Role;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserDTO{

    public Long id;

    public String name;

    private String password;

    private String firstName = "";

    private String lastName = "";

    private Role role = Role.USER;
}

