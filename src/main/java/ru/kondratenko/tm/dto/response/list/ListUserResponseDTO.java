package ru.kondratenko.tm.dto.response.list;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import ru.kondratenko.tm.dto.UserDTO;
import ru.kondratenko.tm.dto.response.ResponseDTO;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class ListUserResponseDTO extends ResponseDTO {
    private UserDTO[] payloadUser;
}
