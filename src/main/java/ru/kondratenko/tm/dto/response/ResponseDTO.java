package ru.kondratenko.tm.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import ru.kondratenko.tm.enumerated.Status;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class ResponseDTO {
    private Status status;
    private String textError;
}
