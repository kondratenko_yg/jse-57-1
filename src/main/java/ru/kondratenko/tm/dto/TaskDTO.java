package ru.kondratenko.tm.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TaskDTO {

    public Long id;

    public String name;

    private String description;

    private Long projectId;

    private Long userId;

    private String deadline;
}

